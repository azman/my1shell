#!/bin/bash

# getlinux
# - written by azman@my1matrix.org

PICK_VERS=
PICK_EXEC=0
LINUX_URL="https://www.kernel.org"

while [ -n "$1" ] ; do
	case $1 in
		-x|--exec) PICK_EXEC=1 ;;
		-s|--stable) PICK_VERS="stable" ;;
		-l|--longterm) PICK_VERS="longterm" ;;
		-*) echo "** Unknown option $1!" && exit 1 ;;
		[0-9]*) PICK_VERS=$1 ;;
		*) echo "** Unknown param $1!" && exit 1 ;;
	esac
	shift
done

get_latest_grep()
{
	local what=$1 ; shift
	local temp=`echo "$@"| grep $what -A2 -m1`
	local vers=`echo "$temp" | sed -n 2p | sed -e 's/^[ \t]*//'`
	vers=`echo $vers | sed -e 's|^[^>]*>[^>]*>\([^<]*\)<.*$|\1|'`
	local when=`echo "$temp" | sed -n 3p | sed -e 's/^[ \t]*//'`
	when=`echo $when | sed -e 's|^[^>]*>\([^<]*\)<.*$|\1|'`
	echo -n "$vers:$when"
}

get_latest()
{
	local pick=$1
	local test=`curl -s -L $LINUX_URL`
	[ -z "$test" ] && return
	[ -z "$pick" -o "$pick" = "stable" ] && get_latest_grep stable "$test"
	[ -z "$pick" ] && echo -n ":"
	[ -z "$pick" -o "$pick" = "longterm" ] && get_latest_grep longterm "$test"
}

show_latest()
{
	local that=`get_latest`
	[ -z "$that" ] && echo "Cannot get info - offline?" && return
	local vers=`echo $that | cut -d':' -f1`
	local when=`echo $that | cut -d':' -f2`
	echo "-- Latest stable version: $vers ($when)"
	vers=`echo $that | cut -d':' -f3`
	when=`echo $that | cut -d':' -f4`
	echo "-- Latest longterm version: $vers ($when)"
}
[ -z "$PICK_VERS" ] && show_latest && exit 0

case $PICK_VERS in
	stable|longterm)
		echo -n "-- Looking for latest $PICK_VERS... "
		PICK_VERS=`get_latest $PICK_VERS | cut -d':' -f1`
		;;
esac

check_kernel_version()
{
	local vers=$1
	local that="${LINUX_URL}/pub/linux/kernel"
	local pick=`echo $vers | cut -d'.' -f1`
	local path="${that}/v${pick}.x/"
	local file="linux-${vers}.tar.xz"
	local full="${path}${file}"
	local test_url="http://www.google.com"
	# check if we are online!
	wget --spider $test_url >/dev/null 2>&1
	[ $? -ne 0 ] && echo "offline;$full" && return
	# check path
	wget --spider $path >/dev/null 2>&1
	[ $? -ne 0 ] && echo -n "ko-path;$full" && return
	# check file
	wget --spider $full >/dev/null 2>&1
	[ $? -ne 0 ] && echo -n "ko-file;$full" || echo -n "ok;$full"
}

setup_kernel_version()
{
	local vers=$1
	local that=`check_kernel_version $vers`
	local test=`echo $that | cut -d';' -f1`
	local full=`echo $that | cut -d';' -f2`
	local path=`dirname $full`
	local file=`basename $full`
	case $test in
		offline) echo -e "\n** Are we offline? Aborting!" ; exit 1 ;;
		ko-path) echo -e "\n** Invalid pick? ($path)" ; exit 1 ;;
		ko-file) echo -e "\n** Not found? ($full)" ; exit 1 ;;
		ok) echo "{$file}" ;;
		*) echo -e "\n** Unknown response? $that" ; exit 1 ;;
	esac
	PICK_FULL=$full
}
setup_kernel_version $PICK_VERS

get_kernel()
{
	local full=$1
	local file=`basename $full`
	[ -f "$file" ] && echo "## File '$file' found." && return
	local name=`basename $file .tar.xz`
	echo -ne "\nDownloading $name:     "
	local opts="--continue --no-use-server-timestamps --progress=dot"
	wget $opts $full 2>&1 | \
		grep --line-buffered "%" | sed -u -e "s,\.,,g" | \
		awk '{printf("\b\b\b\b%4s", $2)}'
	[ ! -r "$file" ] &&
		echo -e "\n** Cannot download source $file!\n" ||
		echo -e "\b\b\b\b DONE!\n"
}
[ $PICK_EXEC -ne 0 ] && get_kernel $PICK_FULL

exit 0
