#!/bin/bash

# cmpstat : written by azman@my1matrix.org
# - compare files in src and dst paths in terms of mtime
# - only if filetype is the same (stat)
# - option to update mtime info
# - use: cmpstat <srcpath> <dstpath> [--swap] [--exec|-x]

must_be_path() {
	local that
	[ -L "$1" ] && that=$(readlink -e $1) || that=$1
	[ ! -d "$that" ] &&
		echo "** $1 not a valid path!" && exit 1
}

get_atime() { # last accessed time
	local temp=$(stat -c '%x' $1)
	echo -n $(date --iso-8601=ns --date="$temp")
}

get_mtime() { # last modified time
	local temp=$(stat -c '%y' $1)
	echo -n $(date --iso-8601=ns --date="$temp")
}

cmpstat() {
	local psrc pdst that temp test chk1 chk2 exec swap
	psrc=$1
	pdst=$2
	[ -z "$psrc" -o -z "$pdst" ] &&
		echo "** Need at least 2 paths!" && exit 1
	shift ; shift

	must_be_path $psrc
	must_be_path $pdst
	psrc=$(cd $psrc;pwd)
	pdst=$(cd $pdst;pwd)

	swap=0 ; exec=0
	while [ ! -z "$1" ] ; do
		case $1 in
			--exec|-x) exec=1 ;;
			--swap) swap=1 ;;
			-*|*) echo "** Unknown option '$1'!" && exit 1 ;;
		esac
		shift
	done
	if [ $swap -ne 0 ] ; then
		temp=psrc
		psrc=pdst
		pdst=temp
	fi

	for that in $(find $psrc/) ; do
		temp=$(realpath -s --relative-to=$psrc $that)
		[ "$temp" = "." -o "$temp" = ".." ] && continue
		test="$pdst/$temp"
		# ignore if not existing in destination
		[ ! -e "$test" ] && continue
		# ignore if not same type (stat)
		chk1=$(stat -c '%A' $that)
		chk2=$(stat -c '%A' $test)
		if [ "$chk1" != "$chk2" ] ; then
			echo "** $temp [$chk1][$chk2]"
			continue
		fi
		# check last modified time
		chk1=$(get_mtime $that)
		chk2=$(get_mtime $test)
		if [ "$chk1" != "$chk2" ] ; then
			#tmp1=$(stat -c '%s' $that) check size?
			#tmp2=$(stat -c '%s' $test)
			if [ $exec -ne 0 ] ; then
				touch -m -d "$chk1" $test
			else
				echo "@@ $temp [$chk1][$chk2]"
			fi
		fi
	done
}
cmpstat $@
