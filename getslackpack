#!/bin/bash

# getslackpack
# - written by azman@my1matrix.org
# - standalone slackware package retriever

TOOL_NAME=$(basename $0)
CURR_PATH=$(pwd)
for CONF in $CURR_PATH/.$TOOL_NAME $HOME/.$TOOL_NAME ; do
	[ -r $CONF ] && . $CONF && break
done
[ -z "$SLACKROOT" ] && SLACKROOT=$CURR_PATH
[ -z "$SLACKARCH" ] && [ -n "$ARCH" ] && SLACKARCH=$ARCH
[ -z "$SLACKARCH" ] && SLACKARCH=$(uname -m)
[ "$SLACKARCH" = "x86_64" ] && SLACKSUFX="64" || SLACKSUFX=
[ -z "$SLACKNAME" ] && SLACKNAME="slackware$SLACKSUFX"
[ -z "$SLACKVERS" ] && [ -n "$VERS" ] && SLACKVERS=$VERS
[ -z "$SLACKVERS" ] && SLACKVERS="current"
SLACKFULL=$SLACKNAME-$SLACKVERS
SLACKPATH=$SLACKROOT/slackpack$SLACKSUFX-$SLACKVERS
# temporary path
TOOL_USER=$(whoami)
TOOL_TEMP="/tmp/$TOOL_USER/$TOOL_NAME"
# x86 packages usually target specific arch - used in salix/rwork repos!
[ -z "$ARCH_X86" ] && ARCH_X86="i486"
case $SLACKARCH in x86|i*86) SLACKARCH=$ARCH_X86 ;; esac
# tool options - allow config file to set these...
OPT_CHECK=${OPT_CHECK:="YES"}
OPT_CLEAN=${OPT_CLEAN:="NO"}
OPT_SWEEP=${OPT_SWEEP:="NO"}
THIS_CONF=${THIS_CONF:=""}

do_download() {
	local file name path base nopt
	file="$1"
	[ -z "$file" ] && exit 1 # shouldn't be here?
	base=$(basename $file)
	name="$2"
	path=$(pwd)
	if [ -z "$name" ] ; then name=$base
	else [ "$name" != "$base" ] && nopt=" --output-document=$name" ; fi
	echo -n "Downloading $name:     "
	wget$nopt --no-use-server-timestamps --progress=dot "$file" 2>&1 | \
		grep --line-buffered "%" | sed -u -e "s,\.,,g" | \
		awk '{printf("\b\b\b\b%4s", $2)}'
	[ ! -r "$name" ] &&
		echo -e "\n** Cannot download source $file to $path/$name!" && exit 1
	echo -ne "\b\b\b\b DONE!\n"
}

go_loadsums() {
	local root repo sums curr temp test that
	root=$1 ; repo=$2 ; sums=$3
	[ -z "$sums" ] && sums="CHECKSUMS.md5"
	# check clean option
	if [ "$OPT_CLEAN" == "YES" ] ; then
		rm -rf $TOOL_TEMP
		[ $? -eq 0 ] && OPT_CLEAN="NO"
	fi
	# prepare temp
	curr=$(pwd)
	temp="$TOOL_TEMP/$repo$SLACKSUFX-$SLACKVERS"
	mkdir -p $temp ; cd $temp
	# if not found OR nothing modified last 120 minutes
	test=$(find $sums -mmin -120 2>/dev/null)
	if [ -z "$test" ]; then
		rm -rf ${sums}* # just in case, delete old ones
		that="$root/$sums"
		wget --spider ${that}.gz >/dev/null 2>&1
		if [ $? -eq 0 ] ; then
			do_download ${that}.gz ${sums}.gz
			gzip -d ${sums}.gz
		else
			do_download $root/$sums $sums
		fi
		# we must have it!
		[ ! -r "$sums" ] && echo "Cannot find $sums!" >&2 && exit 1
	fi
	cd $curr
}

go_loadpkgt() {
	local func root repo pack sums curr temp test that
	func="go_loadpkgt()"
	root=$1 ; repo=$2 ; pack=$3
	[ -z "$pack" ] && pack="PACKAGES.TXT"
	sums=$4
	[ -z "$sums" ] && sums="CHECKSUMS.md5"
	# prepare temp
	curr=$(pwd)
	temp="$TOOL_TEMP/$repo$SLACKSUFX-$SLACKVERS"
	mkdir -p $temp ; cd $temp
	# check if not found OR nothing modified last 24x60 minutes
	test=$(find $pack -mmin -1440 2>/dev/null)
	if [ -z "$test" ]; then
		rm -rf ${pack}* # just in case, delete old one
		that="$root/$pack"
		wget --spider ${that}.gz >/dev/null 2>&1
		if [ $? -eq 0 ] ; then
			do_download ${that}.gz ${pack}.gz
			gzip -d ${pack}.gz
		else
			do_download $that $pack
		fi
		# we must have it!
		[ ! -r "$pack" ] && echo "$func: Cannot find $pack!" >&2 && exit 1
		# check validity
		cat $sums | grep -e " ./${pack}$" >$pack.md5
		if [ -r "$pack.md5" -a "$(cat $pack.md5)" != "" ]; then
			md5sum --check $pack.md5 >/dev/null 2>&1
			[ $? -ne 0 ] && rm -f $pack &&
				echo "$func: [$pack] Checksum failed!" >&2 && exit 1
		else
			echo "$func: $pack validity not checked! (no md5)"
		fi
	fi
	cd $curr
}

go_loadinfo() {
	local root repo sums file
	root=$1 ; repo=$2
	# obtain checksum
	sums="CHECKSUMS.md5"
	go_loadsums $root $repo $sums
	# obtain package
	file="PACKAGES.TXT"
	go_loadpkgt $root $repo $file $sums
}

go_loadpack() {
	local root rsrc repo path file sums mdf5 test name temp that
	root=$1 ; rsrc=$2 ; repo=$3 ; path=$4 ; file=$5
	sums="$TOOL_TEMP/$repo$SLACKSUFX-$SLACKVERS/CHECKSUMS.md5"
	mdf5="$path/$file.md5"
	[ "$rsrc" = "." ] && rsrc=
	# obtain checksum
	go_loadsums $root $repo
	# go to package path
	mkdir -p $path
	cd $path
	# fix download path
	root=${root}${rsrc}
	test=0
	# tell them...
	echo -n "$file"
	# check if valid package exists
	if [ -r $file.md5 ]; then
		md5sum --check $file.md5 >/dev/null 2>&1
		[ $? -eq 0 ] && test=1
		if [ $test -ne 0 ]; then
			echo -n " (LOCAL)"
		else
			if [ -f "${file}" ] ; then
				rm -rf ${file}
				echo -n " ..."
			fi
		fi
	fi
	echo
	# find old files
	name=${file%-*} # strip build
	name=${name%-*} # strip arch
	name=${name%-*} # strip vers
	for temp in ${name}* ; do
		# make sure same package name (not extended)
		that=${temp%-*} # strip build
		that=${that%-*} # strip arch
		that=${that%-*} # strip vers
		[ "$that" != "$name" ] && continue;
		# leave the current ones
		[ "$temp" == "$file" ] && continue;
		[ "$temp" == "$file.md5" ] && continue;
		echo -n "Found '$temp'"
		[ "$OPT_SWEEP" == "NO" ] && echo && continue
		rm -rf $temp
		echo ": REMOVED."
	done
	if [ "$OPT_CHECK" != "YES" ] && [ $test -eq 0 ]; then
		# extract checksum for file
		cat $sums | grep -e "^.*${rsrc}/${file}$" >$mdf5
		[ $? -ne 0 ] && echo "** Cannot create checksum for '$file'!" && exit 1
		# fix path in checksum file
		sed -i "s|^\([A-Za-z0-9]*\) .*$|\1 $file|g" $mdf5
		# download if not found or invalid
		do_download $root/$file $file
		[ ! -r "$file" ] && rm -f $file.md5 &&
			echo "[ERROR] Download error?!" && exit 1
		md5sum --check $file.md5 >/dev/null 2>&1
		if [ $? -ne 0 ] ; then
			rm -f $file $file.md5
			echo "[ERROR] Checksum failed!" && exit 1
		fi
	fi
	cd - >/dev/null
}

go_loadrawl() {
	local func root repo arch temp curr list test
	func="go_loadrawl()"
	root=$1 ; repo=$2 ; arch=$3
	repo="$repo$SLACKSUFX-$SLACKVERS"
	# check clean option
	if [ "$OPT_CLEAN" == "YES" ] ; then
		rm -rf $TOOL_TEMP
		[ $? -eq 0 ] && OPT_CLEAN="NO"
	fi
	# prepare temp
	temp="$TOOL_TEMP/$repo"
	curr=$(pwd)
	mkdir -p $temp ; cd $temp
	list="index.html"
	test=$(find $list -mmin -120 2>/dev/null)
	# if not found OR nothing modified last 120 minutes
	if [ -z "$test" ]; then
		rm -rf ${list}* # just in case, delete old one
		wget --spider $root >/dev/null 2>&1
		[ $? -eq 0 ] && do_download $root $list
		[ ! -r "$list" ] &&
			echo "$func: Cannot get $list for {$repo}!" >&2 && exit 1
	fi
	cd $curr
}

go_packrawl() {
	local root repo path file rsrc test name temp that
	root=$1 ; repo=$2 ; path=$3 ; file=$4 ; rsrc=$5 ; test=0
	# tell them...
	echo -n "$file"
	# prepare / change to pack path
	mkdir -p $path ; cd $path
	if [ -f "$file.md5" ]; then
		md5sum --check $file.md5 >/dev/null 2>&1
		[ $? -eq 0 ] && test=1
		if [ $test -ne 0 ]; then
			echo -n " (LOCAL)"
		else
			if [ -f "$file" ] ; then
				rm -rf $file
				echo -n " ..."
			fi
		fi
	fi
	echo
	# find old files
	name=${file%-*} # strip build
	name=${name%-*} # strip arch
	name=${name%-*} # strip vers
	for that in ${name}* ; do
		# make sure same package name (not extended)
		temp=${that%-*} # strip build
		temp=${temp%-*} # strip arch
		temp=${temp%-*} # strip vers
		[ "$temp" != "$name" ] && continue;
		# leave the current ones
		[ "$that" == "$file" ] && continue;
		[ "$that" == "$file.md5" ] && continue;
		echo -n "Found '$that'"
		[ "$OPT_SWEEP" == "NO" ] && echo && continue
		rm -rf $that
		echo ": REMOVED."
	done
	if [ "$OPT_CHECK" != "YES" ] && [ $test -eq 0 ]; then
		# download if old?
		if [ "$(find $file -mmin -1440 2>/dev/null)" == "" ]; then
			rm -rf $file # just in case, delete old one
			do_download $root/$file $file
			if [ "$rsrc" == "$file.md5" ]; then
				rm -rf $rsrc # just in case, delete old one
				do_download $root/$rsrc $rsrc
			fi
			if [ ! -r "$file" ]; then
				echo "[ERROR] Download Error?!"
			elif [ -r "$rsrc" ]; then
				test="KO"
				md5sum --check $rsrc >/dev/null 2>&1
				[ $? -eq 0 ] && test="OK"
				if [ "$test" != "OK" ]; then
					echo "[ERROR] Checksum failed!" &&
						rm -f $file && rm -f $rsrc
				fi
			fi
		fi
	fi
	cd - >/dev/null
}

go_findrawl() {
	local func repo name arch vers info temp text test file md5s
	func="go_findrawl()"
	[ ! -z "${pack+x}" ] && echo "$func: Var 'pack' NOT defined!" && exit 1
	repo=$1 ; name=$2 ; arch=$3 ; vers=$4
	info="$TOOL_TEMP/$repo$SLACKSUFX-$SLACKVERS/index.html"
	# info structure?
	temp="${name}-[^-]*-${arch}-[^-]*.t[gx]z "
	text="$(cat $info | sed -e :a -e 's/<[^>]*>//g;/</N;//ba')"
	test="$(echo "$text" | grep -e 't[gx]z\s' | grep $name)"
	[ "$test" = "" ] && return
	file=$(echo $test | cut -d' ' -f1)
	[ "$file" = "" ] && return
	# check for md5 file
	temp="${name}-[^-]*-${arch}-[^-]*.t[gx]z.md5"
	test=$(echo "$text" | grep -e "$temp")
	md5s=$(echo $test | cut -d' ' -f1)
	# assign!
	pack="$file:$md5s"
}

go_findpack() {
	local func repo name arch vers skip info temp text test labs file path
	func="go_findpack()"
	[ ! -z "${pack+x}" ] && echo "$func: Var 'pack' NOT defined!" && exit 1
	repo=$1	; name=$2 ; arch=$3 ; vers=$4
	[ "$repo" = "extra" ] && skip="/extra"
	info="$TOOL_TEMP/$repo$SLACKSUFX-$SLACKVERS/PACKAGES.TXT"
	[ ! -f "$info" ] && echo "Cannot find '$info'!" && exit 1
	# info structure
	temp=" ${name}-[^-]*-[^-]*-[^-]*.t[gx]z$"
	text=$(cat $info | grep -e "$temp" -A3)
	[ "$text" = "" ] && return
	# filter arch
	temp=$arch
	#[ "$arch" == "i486" ] && [ "$repo" == "salix" ] && temp="i686"
	[ "$arch" == "i486" ] && temp="i[3456]86"
	test=$(echo "$text" | grep -E "$temp" -A3)
	[ "$test" == "" ] && test=$(echo "$text" | grep -E "noarch" -A3)
	[ "$test" == "" ] && return
	text="$test"
	# filter version
	if [ "$repo" == "alien" -o "$repo" == "alien-res" ] ; then
		temp=pkg$SLACKSUFX/$vers
		test=$(echo "$text" | grep -e "$temp" -A2 -B1)
		[ "$test" == "" ] && return;
		text="$test"
	fi
	# get package name
	labs="PACKAGE NAME:"
	file=$(echo "$text" | grep "^$labs")
	file=$(echo "$file" | sed "s/$labs[[:space:]]*//")
	# get location
	labs="PACKAGE LOCATION:"
	path=$(echo "$text" | grep "^$labs")
	path=$(echo "$path" | sed "s/$labs[[:space:]]*//")
	path=$(echo "$path" | sed "s|^.$skip\(.*\)$|\1|")
	[ "$path" = "" ] && path="."
	# assign!
	pack="$file:$path"
}

do_listrepo() {
	local name temp list
	name=.$TOOL_NAME.repo
	for temp in $CURR_PATH $HOME ; do
		temp=$temp/$name
		[ -f $temp ] && list=$temp && break
	done
	[ ! -r "$list" ] && return
	cat $list | sed -n 's/^\([A-Za-z0-9\-]*=.*\)$/\1/p'
}

do_findrepo() {
	local repo list
	repo=$1
	list=$repo_list
	[ -z "$list" ] && return
	echo "$list"|grep "^$repo="|head -n 1
}

do_siterepo() {
	local find temp xtra
	find=$1
	temp=${find#*=}
	if [ x${temp//|/} != x$temp ] ; then
		xtra=$(echo $temp|cut -d'|' -f2)
		temp=$(echo $temp|cut -d'|' -f1)
		[ "$xtra" != "rawp" ] && xtra=
	fi
	repo=$(eval echo $temp)
	[ ! -z "$xtra" ] && repo="$repo|$xtra"
	echo -n "$repo"
}

do_findthat() {
	local path item repo name rawp root temp arch vers pack file rsrc
	path=$1
	item=$2
	repo=$(echo $item | cut -d':' -f1)
	name=$(echo $item | cut -d':' -f2)
	rawp=0
	root=$(do_findrepo $repo)
	[ -z "$root" ] && echo "** Unknown repo '$repo'!" && exit 1
	root=$(do_siterepo $root)
	if [ x${root//|/} != x$root ] ; then
		temp=$(echo $root|cut -d'|' -f2)
		root=$(echo $root|cut -d'|' -f1)
		[ "$temp" = "rawp" ] && rawp=1
	fi
	arch=$SLACKARCH
	vers=$SLACKVERS
	#echo "[DEBUG] |$root|$repo|$name|$rawp|$arch|$vers|" ; return
	if [ $rawp -eq 0 ]; then
		go_loadinfo $root $repo
		echo -n "{$repo} '$name': "
		go_findpack $repo $name $arch $vers
		if [ ! -z "$pack" ]; then
			file=$(echo $pack | cut -d':' -f1)
			rsrc=$(echo $pack | cut -d':' -f2)
			go_loadpack $root $rsrc $repo $path $file
		else
			echo "not found!"
		fi
	else
		go_loadrawl $root $repo $arch
		echo -n "{$repo} '$name': "
		go_findrawl $repo $name $arch $vers
		if [ ! -z "$pack" ]; then
			file=$(echo $pack | cut -d':' -f1)
			rsrc=$(echo $pack | cut -d':' -f2)
			go_packrawl $root $repo $path $file $rsrc
		else
			echo "not found!"
		fi
	fi
}

create_list() {
	local repo temp find
	repo="alien" # default repo
	# process param and create expanded list
	while [ ! -z "$1" ]; do
		temp=$1
		if [ "${temp:0:2}" = "--" ] ; then
			find=$(do_findrepo ${temp:2})
			[ -z "$find" ] && echo "** Unknown repo '$temp'!" && exit 1
			repo=${temp:2}
		elif [ "${temp:0:1}" = "-" ] ; then
			echo "** Invalid repo '$temp'!" && exit 1
		else
			temp=$repo:$temp
			[ -z "$list" ] && list="$temp" || list="$list;$temp"
		fi
		shift
	done
}

is_repo() {
	local pick repo
	pick=$1
	repo=$(do_findrepo ${pick:2})
	[ -z "$repo" ] && echo "** Invalid repo $pick!" && exit 1
}

getslackpack() {
	local repo temp path repo_list list size step srv1
	repo="--alien" # default repo
	temp="$THIS_CONF"
	path="$SLACKPATH"
	repo_list=$(do_listrepo)
	if [ -z "$repo_list" ] ; then
		srv1="http://mirrors.slackware.com/slackware"
		repo_list="slack=$srv1/$SLACKFULL"
		repo_list="$repo_list\nextra=$srv1/$SLACKFULL/extra"
		srv1="http://download.salixos.org"
		repo_list="$repo_list\nsalix=$srv1/$SLACKARCH/$SLACKVERS"
		repo_list=$(echo -e "$repo_list")
	fi
	while [ "$1" != "" ]; do
		case "$1" in
			--reset) temp= ;;
			--exec) OPT_CHECK="NO" ;; # default is YES (check only)
			--check) OPT_CHECK="YES" ;; # in case need to override config
			--clean) OPT_CLEAN="YES" ;;
			--sweep) OPT_SWEEP="YES" ;;
			--path) shift ; path=$1 ;;
			--*) is_repo $1 ; repo=$1 ;;
			-*) echo "** Unknown option '$1'!" ; exit 1 ;;
			*) temp="$temp $repo $1" ;; # assume a package
		esac
		shift
	done
	[ -z "$temp" ] && exit 0 # nothing to get!
	[ ! -d "$path" ] && echo "-- Creating '$path'" && mkdir -p $path
	path=$(cd $path ; pwd)
	create_list $temp
	size=0
	temp=$(echo $list | tr ';' ' ')
	for step in $temp ; do ((size++)) ; done
	echo "Package Path: $path"
	echo "Package Count: $size"
	for step in $temp ; do do_findthat $path $step ; done
}
getslackpack $@
exit 0
