#!/bin/bash

# chktype
# - written by azman@my1matrix.org
# - create/compile a c program that prints primitive data type sizes
# - removes all created binary/source before exiting

CURR_PATH=$(pwd)
TEMP_PATH="/tmp"
TEMP_CBIN="$(basename $0 .sh)"
TEMP_CSRC="${TEMP_CBIN}.c"

must_be_path()
{
	[ ! -d "$1" ] && echo "** Invalid path '$1'!" && exit 1
}

KEEP_CBIN=0
KEEP_CSRC=0
while [ ! -z "$1" ] ; do
	case $1 in
		--temp) shift ; must_be_path $1 ; TEMP_PATH=$(cd $1 ; pwd) ;;
		--keep-bin) KEEP_CBIN=1 ;;
		--keep-src) KEEP_CSRC=1 ;;
		-*|*) echo "** Unknown option!" ;;
	esac
	shift
done

CC_DETECT=$(which cc)
[ ! -x "$CC_DETECT" ] && echo "** Cannot find C compiler 'cc'!" && exit 1

TEMP_CBIN=$TEMP_PATH/$TEMP_CBIN
TEMP_CSRC=$TEMP_PATH/$TEMP_CSRC

CODE_CSRC=$(cat <<CODELINES
#include <stdio.h>
struct size_item_t {
	char name[12];
	size_t size;
};
struct size_item_t sizelist[] =
{
	{"char",sizeof(char)},
	{"int",sizeof(int)},
	{"short",sizeof(short)},
	{"long",sizeof(long)},
	{"long long",sizeof(long long)},
	{"float",sizeof(float)},
	{"double",sizeof(double)},
	{"long double",sizeof(long double)},
	{"void*",sizeof(void*)}
};
int sizesize = sizeof(sizelist)/sizeof(struct size_item_t);
int main(void)
{
	int loop;
	struct size_item_t* temp;
	printf("-----------------\\\\n");
	printf("C data type sizes\\\\n");
	printf("-----------------\\\\n");
	for (loop=0;loop<sizesize;loop++)
	{
		temp = &sizelist[loop];
		printf("Type:{%12s},Size:[%d]\\\\n",temp->name,temp->size);
	}
	return 0;
}
CODELINES
)

echo -e "$CODE_CSRC" > ${TEMP_CSRC}
$CC_DETECT -o ${TEMP_CBIN} ${TEMP_CSRC}
[ $? -ne 0 ] && echo "** Cannot compile ${TEMP_CSRC}!" && exit 1
${TEMP_CBIN}
[ $KEEP_CBIN -ne 0 ] && echo "## Keeping ${TEMP_CBIN}" || rm -rf ${TEMP_CBIN}
[ $KEEP_CSRC -ne 0 ] && echo "## Keeping ${TEMP_CSRC}" || rm -rf ${TEMP_CSRC}

exit 0
